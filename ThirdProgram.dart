class App {
  App(this.Appname,this.Category,this.Year,this.developer);
  final String Appname;
  final String Category;
  final int Year;
  final String developer;
}

main() {
  List Winners = [];
  Winners.add(App("Ambani Africa","Best Educational Solution" ,2021,"Mukundi Lambani"));
  Winners.add(App("EasyEquities","Best Consumer Solution" ,2020,"EasyEquities Team"));
  Winners.add(App("Naked","Best Financial Solution",2019,"Alex Thomson"));
  Winners.add(App("Khula Ecosystem","Best Agriculture Solution" ,2018,"Karidas Tshintsholo"));
  Winners.add(App("Shyft","Best Financial Solution" ,2017,"Standard Bank Team"));
  Winners.add(App("Domestly","Best Consumer App" ,2016,"Thatoyoana Marumo"));
  Winners.add(App("WumDrop","Best Enterprise" ,2015,"Benjamin Claassen"));
  Winners.add(App("Live Inspect","App of the Year" ,2014,"Lightstone"));
  Winners.add(App("SnapScan", "Best HTML 5 app",2013,"Kobus Ehlers"));
  Winners.add(App("FNB Banking","App of the Year" ,2012,"FNB team"));

for ( var Apps in Winners){
  print("App of the year winner : "+Apps.Appname + " in the "+Apps.Category +" Category, in the year "+Apps.Year.toString() + " App Developed by "+Apps.developer);
  print("*******************************************");
  print(Apps.Appname.toUpperCase());
}

}

